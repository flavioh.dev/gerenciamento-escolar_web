# Gerenciamento Escolar - API

Projeto criado para o Trabalho de Conclusão de Curso de Tecnólogo em Análise e Desenvolvimento de Sistemas da FATEC Ribeirão Preto.<br/>

Foi idealizado com o intuito de auxiliar a gestão de matrículas da escola onde trabalho, a EMEB "Maria Virgínia Matarazzo Ippólito", 
uma escola municipal de 1º ao 5º ano do ensino fundamental situada em Cravinhos-SP.<br/>

Foi baseado no projeto [Proffy](https://github.com/rocketseat-education/nlw-02-omnistack) da [Rocketseat](https://github.com/rocketseat-education).

API do projeto: [Gerenciamento Escolar - API](https://github.com/flavioh-assis/gerenciamento-escolar_web)

# Refatorações

Em 11/02/2024 foi iniciado a fase de refatoração do projeto, visando colocar em prática o que eu aprendi desde a última atualização do projeto em 09/06/2021.<br/>

Todo front-end foi movido do projeto [Gerenciamento Escolar](https://github.com/flavioh-assis/gerenciamento-escolar_web) para esse repositório em 28/05/2024.

Alterações realizadas:
 * Em breve

# Tecnologias

  * TypeScript
  * ReactJS
  * Redux
  * CSS
  * Node.js
  * Postgres
  * Express
  * Knex
  * Axios
 
# Links úteis
 
 * [Deploy do Projeto](https://gerenciamento-escolar.vercel.app/)
 * [Artigo WorkTec](https://docs.google.com/document/d/1xyH6Z3J6rALaGfIFmGBaaJziCJi1UOFH/edit?usp=sharing&ouid=105664717359710468557&rtpof=true&sd=true)
 * [Apresentação](https://docs.google.com/presentation/d/1V2H3M3n8rAYSfewMI12OvwiFNAZgDegr/edit?usp=sharing&ouid=105664717359710468557&rtpof=true&sd=true)

# Como Executar

 * Pré-requisitos
   * É necessário possuir o Node.js >= v16 instalado no computador
   * É necessário possuir o Git instalado e configurado no computador
   
1. Faça um clone do repositório:
   ```sh
    git clone https://github.com/flavioh-assis/gerenciamento-escolar_web.git
   ```
2. Executando a Aplicação:
   ```sh
    # Instalando as dependências do projeto.
    npm install
    # Inicie a aplicação web para o desenvolvimento
    npm run dev
    ```
