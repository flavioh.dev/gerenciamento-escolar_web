import React from 'react';
import { GridColDef, DataGrid, GridCellParams } from '@material-ui/data-grid';
import { useNavigate } from 'react-router-dom';
import GoToIcon from '../../../assets/images/icons/go_to_icon.png';
import { StudentResponse } from '../../../types';
import './styles.css';

type Props = {
  students: StudentResponse[];
};

export const TableStudents = ({ students }: Props) => {
  const navigate = useNavigate();

  const buildColumn = (
    field: string,
    headerName: string,
    flex: number,
    renderCell?: (params: GridCellParams) => React.ReactNode,
    hide?: boolean
  ) => {
    return {
      field,
      headerName,
      flex,
      renderCell,
      hide,
      align: 'center',
      headerAlign: 'center',
      sortable: false,
    } as GridColDef;
  };

  const editStudentCallBack = ({ getValue, id }: GridCellParams) => {
    const onClick = () => {
      const idStudent = Number(getValue(id, 'id')) || 0;

      navigate(`/aluno/${idStudent}`);
    };

    return (
      <span className="go_to" onClick={onClick}>
        <img alt="Editar dados do aluno" className="go_to_icon" src={GoToIcon} />
      </span>
    );
  };

  const columns: GridColDef[] = [
    buildColumn('id', 'id', 0, undefined, true),
    buildColumn('num_chamada', 'Nº', 0.5),
    buildColumn('nome', 'Nome Completo', 3),
    buildColumn('ra', 'RA', 1.1),
    buildColumn('nasc_data', 'Nasc.', 1),
    buildColumn('situacao', 'Situação', 1),
    buildColumn('nee', 'Deficiência', 1),
    buildColumn('editStudentAction', ' ', 0.5, editStudentCallBack),
  ];

  return (
    <div id="mostra-alunado">
      <DataGrid
        rows={students}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        rowHeight={35}
        autoHeight
        isRowSelectable={() => false}
        disableColumnMenu
      />
    </div>
  );
};
