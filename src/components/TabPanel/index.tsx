import React from 'react';
import { Box, Typography } from '@material-ui/core';

type TabPanelProps = {
  children?: React.ReactNode;
  index: number;
  value: number;
};

const TabPanel = ({ children, value, index, ...other }: TabPanelProps) => (
  <div
    role="tabpanel"
    hidden={value !== index}
    id={`scrollable-auto-tabpanel-${index}`}
    aria-labelledby={`scrollable-auto-tab-${index}`}
    {...other}
  >
    <Box p={0}>
      <Typography component={'div'}>{children}</Typography>
    </Box>
  </div>
);

export { TabPanel };
