import React, { useState } from 'react';
import { AppBar, Tab, Tabs } from '@material-ui/core';
import { TabPanel } from '../TabPanel';
import { TableStudents } from '../../components/BD/MostraAlunado';
import { ClassResponse, StudentResponse } from '../../types';
import { useStyles, tabStyle } from './styles';

type Props = {
  classes: ClassResponse[];
  students: StudentResponse[];
};

export const TabsClasses = ({ classes, students }: Props) => {
  const classesStyle = useStyles();
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);

  const selectedGrade = classes[selectedTabIndex]?.ano;
  const selectedGroup = classes[selectedTabIndex]?.turma;

  const studentsData = students.filter(x => x.ano === selectedGrade && x.turma === selectedGroup);

  const handleChange = (_: any, index: number) => {
    setSelectedTabIndex(index);
  };

  const getLabelText = (grade: string, group: string) => {
    return `${grade} ano ${group}`;
  };

  return (
    <div className={classesStyle.root}>
      <AppBar position="static" color="primary">
        <Tabs
          value={selectedTabIndex}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="inherit"
          variant="scrollable"
          scrollButtons="auto"
        >
          {classes.map(({ ano, turma }) => (
            <Tab key={getLabelText(ano, turma)} label={getLabelText(ano, turma)} style={tabStyle} />
          ))}
        </Tabs>
      </AppBar>

      <TabPanel value={selectedTabIndex} index={selectedTabIndex}>
        <TableStudents students={studentsData} />
      </TabPanel>
    </div>
  );
};
