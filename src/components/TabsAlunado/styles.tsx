import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: '#bbb',
    flexGrow: 1,
    margin: '0 auto',
    width: '100rem',
  },
}));

export const tabStyle = {
  backgroundColor: '#fff',
  color: '#000',
  font: '700 2rem Archivo',
};
