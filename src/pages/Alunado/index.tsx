import React, { useEffect, useState } from 'react';
import Menu from '../../components/Menu';
import Cabeçalho from '../../components/Cabeçalho';
import { TabsClasses } from '../../components/TabsAlunado';
import { getClasses } from '../../services/class-service';
import { getStudents } from '../../services/student-service';
import { ClassResponse, StudentResponse } from '../../types';
import './styles.css';

const StudentsPerClass = () => {
  const [classes, setClasses] = useState<ClassResponse[]>([]);
  const [students, setStudents] = useState<StudentResponse[]>([]);

  const updateClasses = async () => {
    const classes = await getClasses();

    setClasses(classes);
  };

  const updateStudents = async () => {
    const students = await getStudents();

    setStudents(students);
  };

  useEffect(() => {
    updateClasses();
    updateStudents();
  }, []);

  return (
    <div className="container">
      <Cabeçalho titulo="Alunado Classe" endereço="Aluno > Alunado" />

      <div className="menu-conteudo">
        <Menu />

        <div className="alunado">
          <TabsClasses classes={classes} students={students} />
        </div>
      </div>
    </div>
  );
};

export default StudentsPerClass;
