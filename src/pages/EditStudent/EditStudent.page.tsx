import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { ErrorPage } from '../../pages';
import { getClasses } from '../../services/class-service';
import { getStudentById, updateStudent } from '../../services/student-service';
import { DadosAlunos } from '../../components/DadosAlunos';
import { FormEditStudentValues, StudentResponse } from '../../types';
import Cabeçalho from '../../components/Cabeçalho';
import Menu from '../../components/Menu';
import './styles.css';

export const EditStudent = () => {
  const { id } = useParams();
  const numericId = Number(id);

  const [classes, setClasses] = useState<Object[]>([]);
  const [student, setStudent] = useState<StudentResponse>();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData = {} as FormEditStudentValues;
    const elements = e.currentTarget.elements;

    for (let i = 0; i < elements.length; i++) {
      const element = elements[i] as HTMLInputElement | HTMLSelectElement;

      if (element.id) {
        formData[element.id as keyof FormEditStudentValues] = element.value;
      }
    }

    updateStudent(numericId, formData)
      .then(() => {
        alert('Dados atualizados com sucesso!');
      })
      .catch(error => {
        alert(`Deu ruim: ${error}`);
      });
  };

  useEffect(() => {
    const classes = async () => {
      const result = await getClasses();
      setClasses(result);
    };

    const student = async () => {
      const result = await getStudentById(numericId);

      if (Object.keys(result).length > 0) {
        setStudent(result);
      }
    };

    classes();
    student();
  }, []);

  if (student) {
    return (
      <div className="container">
        <Cabeçalho titulo="Editar Aluno" endereço="Aluno > Editar" />

        <div className="menu-conteudo">
          <Menu />

          <div className="edit-student">
            <DadosAlunos classes={classes} student={student} handleSubmit={handleSubmit} />
          </div>
        </div>
      </div>
    );
  }

  return <ErrorPage />;
};
