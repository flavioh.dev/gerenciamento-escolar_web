import React from 'react';

export const ErrorPage = () => (
  <div
    style={{
      display: 'flex',
      background: '#900',
      flex: '1',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      color: 'black',
      fontSize: '5rem',
    }}
  >
    <p>Estudante não existe.</p>
  </div>
);
