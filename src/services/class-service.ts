import { api } from './api';
import { ClassResponse } from '../types';

export const getClasses = async () => {
  const { data: classes } = await api.get('/classes').catch(() => ({
    data: [],
  }));

  return classes as ClassResponse[];
};
