import { api } from './api';
import { FormEditStudentValues, StudentResponse } from '../types';

export const getStudents = async () => {
  const { data: students } = await api.get('/alunos').catch(() => ({
    data: [],
  }));

  return students as StudentResponse[];
};

export const getStudentById = async (id: number) => {
  const { data: student } = await api.get(`/alunos/${id}`).catch(() => ({
    data: {},
  }));

  return student as StudentResponse;
};

export const updateStudent = async (id: number, data: FormEditStudentValues) => {
  return api.put(`/alunos/${id}`, data);
};
