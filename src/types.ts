export type Class = Fields & { id: number };

export type ClassResponse = {
  id: number;
  ano: string;
  turma: string;
  sala: string;
  professor: string;
  periodo: string;
  ano_letivo: string;
  n_ativos: number;
  n_total: number;
};

export type StudentResponse = {
  id: number;
  nome: string;
  ano: string;
  turma: string;
  periodo: string;
  ra: string;
  professor: string;
  situacao: string;
  nasc_data: string;
  nee: string;
  num_chamada: number;
  nasc_cidade: string;
  nasc_uf: string;
  nacionalidade: string;
  pai: string;
  mae: string;
  responsavel: string;
  ano_desejado: string;
};

export type Fields = {
  grade: string;
  group: string;
  period: Period | '';
  classroom: string;
  teacher: string;
};

export type Period = 'Manhã' | 'Tarde';

export type FormStudentValues = {
  nome: string;
  ra: string;
  nasc_cidade: string;
  nasc_uf: string;
  nacionalidade: string;
  nasc_data: string;
  nee: string;
  pai: string;
  mae: string;
  responsavel: string;
  ano_desejado: string;
  turma: string;
};

export type FormEditStudentValues = Omit<
  FormStudentValues,
  'ano_desejado' | 'turma' | 'num_chamada'
>;

export type SearchStudentResult = {
  id: number;
  num_chamada: number;
  nome: string;
  ra: string;
  nee: string;
  nasc_data: string;
  ano: string;
  turma: string;
  professor: string;
  situacao: string;
  periodo: string;
};

export type StudentFilter = {
  nome: string;
  ra: string;
  nee: string;
  ano: string;
  turma: string;
  professor: string;
  periodo: string;
};
